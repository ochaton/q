package main

import (
	"github.com/tarantool/go-tarantool"
	"log"
	"time"
)

type Task struct {
	Id      uint64
	Cid     uint64
	Ctime   int64
	Payload string
}
type Response = []Task

func main() {
	tnt, err := tarantool.Connect("127.0.0.1:3301", tarantool.Opts{
		Timeout: 3 * time.Second,
		Reconnect: 100 * time.Millisecond,
		MaxReconnects: 100,
	})
	if err != nil {
		log.Panic(err)
	}

	var last_id uint64 = 0
	for i := 0; i < 100000; i++ {
		var r []Response
		err := tnt.Call17Typed("subscribe", []interface{}{ last_id }, &r)
		if err != nil {
			log.Panic(err)
		}
		if len(r) > 0 && len(r[0]) > 0 {
			tasks := r[0]
			now := time.Now().UnixNano()
			last_id = tasks[len(tasks)-1].Id

			var mctime int64 = now
			for _, task := range tasks {
				if task.Ctime < mctime {
					mctime = task.Ctime
				}
			}
			latency := float64(now-mctime)/float64(time.Millisecond)
			if latency > 3 {
				log.Printf("[%d] Diff: %03d %.3fms\n", 0, len(tasks), latency)
			}
		}
	}
}