#!/usr/bin/env tarantool
require 'fio'.mkdir('tnt/')

box.cfg{
	listen = 3301,
	readahead = 16*2^20,

	net_msg_max = 8192,
	work_dir = 'tnt/',
}
box.schema.user.grant('guest', 'super', nil, nil, { if_not_exists = true })

fiber = require 'fiber'

box.schema.space.create('q', {
	-- temporary = true,
	if_not_exists = true,
})

box.space.q:create_index('primary', {
	if_not_exists = true,
})

box.space.q:truncate()
box.snapshot()

local cond = fiber.cond()
local id = 0

local chan = fiber.channel(50)

for w = 1, 10 do
	fiber.create(function()
		while true do
			local message = chan:get()
			if message then
				box.begin()
					box.space.q:insert(message)
					while not chan:is_empty() do
						message = chan:get(0)
						box.space.q:insert(message)
					end
					cond:broadcast()
				box.commit()
			end
		end
	end)
end

function push(...)
	id = id + 1
	local message = { id, ... }
	if chan:is_full() then
		cond:broadcast()
		box.space.q:insert(message)
	else
		chan:put(message)
	end
	return
end

function subscribe(start, to)
	to = math.min(tonumber(to) or 1, 1)
	if id == start then
		cond:wait(to)
	end
	return box.space.q:pairs(start, { iterator = box.index.GT }):take_n(1000):totable()
end

require 'console'.start() os.exit()