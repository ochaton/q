package main

import (
	"github.com/tarantool/go-tarantool"
	"log"
	"time"
	"sync"
)

type Task struct {
	Id      uint64
	Ctime   int64
	Payload string
}

func main() {
	tnt, err := tarantool.Connect("127.0.0.1:3301", tarantool.Opts{
		Timeout: 500 * time.Millisecond,
		Reconnect: 100 * time.Millisecond,
		MaxReconnects: 100,
	})
	if err != nil {
		log.Panic(err)
	}

	wg := sync.WaitGroup{}

	var N, W, w uint64 = 1e6, 100, 0

	for w = 1; w <= W; w++ {
		wg.Add(1)
		go func(w uint64) {
			defer wg.Done()

			var i uint64
			for i = 0; i < N/W; i++ {
				if _, err := tnt.Call17("push", []interface{}{ i, time.Now().UnixNano(), "Message" }); err != nil {
					log.Panic(err)
				}				
				if i % 1000 == 0 {
					log.Printf("[%d] Finished %d/%d %.2f%%\n", w, i, N, float64(i)/float64(N)*100)
				}
			}
		}(w)
	}

	wg.Wait()
	log.Println("Finish work")
}